################################################################################
#  This script creates procedures for table formatting and front/back fills, maps
#  countries to FAO codes, and loads area data from FAO, CORINE, GAEZ, and GLC.
#  Population data is used to estimate area for artificial surfaces.
#
#
#	/* To update file paths, find/replace "Evan Neill.ECOFOOT" and "Mikel Evans" with your computer's name */
#
#  Loads infiles:
#		"C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\ForestStat_1961_1990.csv"
#		"C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\Inputs_LandUse_E_All_Data_(Normalized).csv"
#		"C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\CorineLand1990_2019.csv"
#		"C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\CorineLand2000_2019.csv"
#		"C:\\Users\\Swiader\\Documents\\IDS NFA\NFA 2020\\2. datasets\\land\\Upload Data\\CorineLand2006_2019.csv"
#		"C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\GAEZ_raw_19.csv"
#		"C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\GLC_raw.csv"
#
#
#  OUTPUT TABLES
#   `area_20_ids_MS_raw`.`corine_1990_raw`			  #UPDATE
#   `area_20_ids_MS_raw`.`corine_2000_raw` 			  #UPDATE
#   `area_20_ids_MS_raw`.`corine_2006_raw` 			  #UPDATE
#   `area_20_ids_MS_raw`.`resourcestat_raw`           #UPDATE
#   `area_20_ids_MS_raw`.`gaez_raw`                    #UPDATE
#   `area_20_ids_MS_raw`.`ForestStat_1961_1990`       #UPDATE
#   `area_20_ids_MS_raw`.`glc_raw`                    #UPDATE
#   `area_20_ids_MS_sql`.`1990_outlier_values`        #UPDATE
#   `area_20_ids_MS_sql`.`2006_outlier_values`        #UPDATE
#   `area_20_ids_MS_sql`.`bel_lux_ratio_tmp`          #UPDATE
#   `area_20_ids_MS_sql`.`china_splits`               #UPDATE
#   `area_20_ids_MS_sql`.`corine_1990_grouped`        #UPDATE
#   `area_20_ids_MS_sql`.`corine_1990_trpse`          #UPDATE
#   `area_20_ids_MS_sql`.`corine_2000_grouped`        #UPDATE
#   `area_20_ids_MS_sql`.`corine_2000_trpse`          #UPDATE
#   `area_20_ids_MS_sql`.`corine_2006_grouped`        #UPDATE
#   `area_20_ids_MS_sql`.`corine_2006_trpse`          #UPDATE
#   `area_20_ids_MS_sql`.`corine_penult`              #UPDATE
#   `area_20_ids_MS_sql`.`foreststat_1961_1990_sql`   #UPDATE
#   `area_20_ids_MS_sql`.`foreststat_1961_1990_trpse` #UPDATE
#   `area_20_ids_MS_sql`.`gaez_penult`                #UPDATE
#   `area_20_ids_MS_sql`.`glc_penult`                 #UPDATE
#   `area_20_ids_MS_sql`.`not_in_frstat61_90`         #UPDATE
#   `area_20_ids_MS_sql`.`not_in_frstat61_90_regr`    #UPDATE
#   `area_20_ids_MS_sql`.`resourcestat`               #UPDATE
#   `area_20_ids_MS_sql`.`yearval`                    #UPDATE
#   `area_20_ids_MS_ult`.`corine`                     #UPDATE
#   `area_20_ids_MS_ult`.`gaez`                       #UPDATE
#   `area_20_ids_MS_ult`.`glc`                        #UPDATE
#   `area_20_ids_MS_ult`.`resourcestat`               #UPDATE
#   `area_20_ids_MS_ult_hketc`.`corine`               #UPDATE
#   `area_20_ids_MS_ult_hketc`.`gaez`                 #UPDATE
#   `area_20_ids_MS_ult_hketc`.`glc`                  #UPDATE
#   `area_20_ids_MS_ult_hketc`.`resourcestat`         #UPDATE
#
#  Input TABLES
#    `country_data.country_matrix_2018`
#	 `faostat_18_v1_ult`.`popstat_ult`			  #UPDATE
#	 `faostat_18_v1_ult_hketc`.`popstat_ult`	  #UPDATE
#
#  Flags
#   E:  Expert sources from FAO (including other divisions)                                                                        #UPDATE
#   F:  FAO estimate                                                                                                               #UPDATE
#   Fm: Manual Estimation                                                                                                          #UPDATE
#   I:  Country data reported by International Organizations where the country is a member (Semi-official) - WTO, EU, UNSD, etc.   #UPDATE
#   Q:  Official data reported on FAO Questionnaires from countries                                                                #UPDATE
#   W:  Data reported on country official publications or web sites (Official) or trade country files                              #UPDATE
#
#  GFN_flags
#	OUT: value had been flagged as exceptional and replaced
#	FIL: value interpolated or extrapolated
#	R: real value reported by corine
################################################################################

TEE C:\Users\Swiader\Documents\IDS NFA\NFA 2020\2. datasets\land\Upload Data\dot out log files\Landarea_v3_orinda.out

CREATE SCHEMA IF NOT EXISTS `area_20_ids_MS_raw`;			#UPDATE
CREATE SCHEMA IF NOT EXISTS `area_20_ids_MS_sql` ;			#UPDATE
CREATE SCHEMA IF NOT EXISTS `area_20_ids_MS_ult` ;			#UPDATE
CREATE SCHEMA IF NOT EXISTS `area_20_ids_MS_ult_hketc` ;		#UPDATE

USE `area_20_ids_MS_sql` ;									#UPDATE
SET @total_time=UNIX_TIMESTAMP() ;
SELECT NOW() as 'Data Load Started' ;

###---------------------------------------------------------------###
###---------------------------------------------------------------###
### STEP 1: Create Procedures ------------------------------------###
###---------------------------------------------------------------###
###---------------------------------------------------------------###

################################################################################
#1. Re-format raw data tables
################################################################################

	DROP PROCEDURE IF EXISTS  `FormatTables`;

DELIMITER $$
	CREATE PROCEDURE FormatTables(IN `first_year` INT, IN `last_year` INT , IN `query` VARCHAR(2500))
	DETERMINISTIC
	 BEGIN

		DECLARE counter INT DEFAULT 0;
		SET counter=first_year;


			IF first_year <= last_year THEN


				SIMPLE_LOOP: LOOP
					# Do the work
					#SELECT counter as 'Processing';

					SET @QRY = REPLACE(query,'$YEAR$', counter);
					#SELECT @QRY as 'Executing Query';

					PREPARE stmt FROM @QRY;
					EXECUTE stmt;
					# end of do the work

				SET counter = counter +1;
				IF counter > last_year THEN
					LEAVE SIMPLE_LOOP;
				END IF;
				END LOOP SIMPLE_LOOP;
				SELECT 'Finished';
			ELSE
				SELECT 'Sorry you have entered invalid years';
			END IF;

	 END$$

	delimiter ;


################################################################################
#3. Format corine dataset
################################################################################

DROP PROCEDURE IF EXISTS  `Format_Corine_Tables`;

DELIMITER $$

CREATE PROCEDURE `Format_Corine_Tables`(IN `first_year` INT, IN `last_year` INT , IN `query` VARCHAR(2500))
    DETERMINISTIC
BEGIN

  DECLARE counter INT DEFAULT 0;
  DECLARE colname varchar(50);
  SET counter=first_year;

   IF first_year <= last_year THEN
    SIMPLE_LOOP: LOOP
     # Do the work
     SELECT counter as 'Processing';

   	IF counter = 1 then
		set colname = 'Artificial surfaces' ;
	ELSEIF counter = 2 then
		set colname = 'Arable land + Permanent crops';
	ELSEIF counter = 3 then
		set colname = 'Pastures + Heterogeneous agricultural areas';
	ELSEIF counter = 4 then
		set colname = 'Forests';
	ELSE
		set colname = 'Inland waters';
	END IF;

     SET @QRY = REPLACE(query,'$YEAR$', colname);
     SELECT @QRY as 'Executing Query';

     PREPARE stmt FROM @QRY;
     EXECUTE stmt;
     # end of do the work

    SET counter = counter +1;
    IF counter > last_year THEN
     LEAVE SIMPLE_LOOP;
    END IF;
    END LOOP SIMPLE_LOOP;
    SELECT 'Finished';
   ELSE
    SELECT 'Sorry you have entered invalid years';
   END IF;

  END$$
 delimiter ;


################################################################################
#3. Front-fill table to end_year, by country
################################################################################



DROP PROCEDURE IF EXISTS  `FrontFillbyCountry`\p;

	delimiter $$

CREATE PROCEDURE FrontFillbyCountry(IN `max_year_query` VARCHAR(2500), IN `function_query` VARCHAR(2500), IN `country_matrix` VARCHAR(500))
	BEGIN
    SET @country_query = CONCAT('SET @country = (SELECT a.country_code from (SELECT distinct country_code, @row_num := @row_num+1 as row_num
			FROM ', country_matrix, ' WHERE country_code IS NOT NULL ORDER BY country_code asc) a WHERE a.row_num = @selector);');
    SET @end_year_query = CONCAT('SET @end_year = (SELECT end_year FROM ', country_matrix, ' WHERE country_code = @country);');
    SET @selector = 1;
	SET @row_num = 0;
	PREPARE stmt FROM @country_query;
	EXECUTE stmt;
	WHILE @country <= 5001 DO
        PREPARE stmt FROM @end_year_query;
        EXECUTE stmt;
        SET @max_year_query = max_year_query;
		PREPARE stmt from @max_year_query;
        EXECUTE stmt;
		SIMPLE_LOOP: LOOP
			IF @max_year >= @end_year OR @max_year IS NULL THEN
				LEAVE SIMPLE_LOOP;
			END IF;
            SET @QRY = function_query;
            PREPARE stmt FROM @QRY;
            EXECUTE stmt;
            SET @max_year = @max_year+1;
            ITERATE SIMPLE_LOOP;
		END LOOP SIMPLE_LOOP;
		SET @selector = @selector+1;
        SET @row_num = 0;
		PREPARE stmt FROM @country_query;
        EXECUTE stmt;
		END WHILE;
	 END$$

delimiter ; \p;


################################################################################
#4. Back-fill table to end_year, by country
################################################################################
DROP PROCEDURE IF EXISTS  `BackFillbyCountry`;

	delimiter $$

CREATE PROCEDURE BackFillbyCountry(IN `min_year_query` VARCHAR(2500), IN `function_query` VARCHAR(2500), IN `country_matrix` VARCHAR(500))
	BEGIN
    SET @country_query = CONCAT('SET @country = (SELECT a.country_code from (SELECT distinct country_code, @row_num := @row_num+1 as row_num
			FROM ', country_matrix, ' WHERE country_code IS NOT NULL ORDER BY country_code asc) a WHERE a.row_num = @selector);');
    SET @start_year_query = CONCAT('SET @start_year = (SELECT start_year FROM ', country_matrix, ' WHERE country_code = @country);');
	SET @selector = 1;
	SET @row_num = 0;
	PREPARE stmt FROM @country_query;
	EXECUTE stmt;
	WHILE @country <=5001 DO
        PREPARE stmt FROM @start_year_query;
        EXECUTE stmt;
        SET @min_year_query = min_year_query;
		PREPARE stmt from @min_year_query;
        EXECUTE stmt;
        SIMPLE_LOOP: LOOP
            IF @min_year <= @start_year OR @min_year IS NULL THEN
				LEAVE SIMPLE_LOOP;
			END IF;
            SET @QRY = function_query;
			PREPARE stmt FROM @QRY;
			EXECUTE stmt;
            SET @min_year = @min_year-1;
            ITERATE SIMPLE_LOOP;
		END LOOP SIMPLE_LOOP;
		SET @selector = @selector+1;
        SET @row_num = 0;
		PREPARE stmt FROM @country_query;
        EXECUTE stmt;
		END WHILE;
	 END$$
delimiter ; \p;

###---------------------------------------------------------------###
###---------------------------------------------------------------###
### STEP 2: Resourcestat -----------------------------------------###
###---------------------------------------------------------------###
###---------------------------------------------------------------###


#Populate variables for later use
SET @msg = 'ERROR: UNMAPPED COUNTRY';

	SELECT MAX(end_year) FROM country_data.country_matrix_2018 INTO @default_end_year\p;		#UPDATE
	SELECT start_year FROM country_data.country_matrix_2018 WHERE country_code = 276 INTO @Sudan_start_year\p;	  	#UPDATE
	SELECT end_year FROM country_data.country_matrix_2018 WHERE country_code = 276 INTO @Sudan_end_year\p;			#UPDATE
	SELECT start_year FROM country_data.country_matrix_2018 WHERE country_code = 277 INTO @South_Sudan_start_year\p;	#UPDATE
	SELECT end_year FROM country_data.country_matrix_2018 WHERE country_code = 277 INTO @South_Sudan_end_year\p;	#UPDATE

#########################################################################################################
### Create a table with all the years as rows. This table built as a variable year to use for indexing
#########################################################################################################

DROP TABLE IF EXISTS `yearval`\p;
create table `yearval` (
year int(5)
)\p;

INSERT INTO yearval Values
(1961),(1962),(1963),(1964),(1965),(1966),(1967),(1968),(1969),(1970),
(1971),(1972),(1973),(1974),(1975),(1976),(1977),(1978),(1979),(1980),
(1981),(1982),(1983),(1984),(1985),(1986),(1987),(1988),(1989),(1990),
(1991),(1992),(1993),(1994),(1995),(1996),(1997),(1998),(1999),(2000),
(2001),(2002),(2003),(2004),(2005),(2006),(2007),(2008),(2009),(2010),
(2011),(2012), (2013),(2014), (2015)\p;																#UPDATE

#########################################################################################################
### upload old forest data now removed from the official FAO data set. However it has been used to estimate forest land use changes from 1961 to 1989.
###Source: Data extracted from MySQL on August 9th 2007 from the faostat_07 resourcestat_07 section.
#########################################################################################################
DROP TABLE IF EXISTS `area_20_ids_MS_raw`.`ForestStat_1961_1990`\p;
CREATE TABLE  `area_20_ids_MS_raw`.`ForestStat_1961_1990`(
  `country` varchar(41) NOT NULL,
  `item` varchar(40) NOT NULL,
	`1961` double DEFAULT NULL,`1962` double DEFAULT NULL,`1963` double DEFAULT NULL,`1964` double DEFAULT NULL,
	`1965` double DEFAULT NULL,`1966` double DEFAULT NULL,`1967` double DEFAULT NULL,`1968` double DEFAULT NULL,
	`1969` double DEFAULT NULL,`1970` double DEFAULT NULL,`1971` double DEFAULT NULL,`1972` double DEFAULT NULL,
	`1973` double DEFAULT NULL,`1974` double DEFAULT NULL,`1975` double DEFAULT NULL,`1976` double DEFAULT NULL,
	`1977` double DEFAULT NULL,`1978` double DEFAULT NULL,`1979` double DEFAULT NULL,`1980` double DEFAULT NULL,
	`1981` double DEFAULT NULL,`1982` double DEFAULT NULL,`1983` double DEFAULT NULL,`1984` double DEFAULT NULL,
	`1985` double DEFAULT NULL,`1986` double DEFAULT NULL,`1987` double DEFAULT NULL,`1988` double DEFAULT NULL,
	`1989` double DEFAULT NULL,`1990`double DEFAULT NULL,
PRIMARY KEY  (`country`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1\p;

LOAD DATA LOCAL INFILE "C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\ForestStat_1961_1990.csv"		#UPDATE
INTO TABLE `area_20_ids_MS_raw`.`ForestStat_1961_1990` CHARACTER SET  latin1						#UPDATE
FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY "\r\n" IGNORE 1 LINES
(country,item,@1961,@1962,@1963,@1964,@1965,@1966,@1967,@1968,@1969,
@1970,@1971,@1972,@1973,@1974,@1975,@1976,@1977,@1978,@1979,
@1980,@1981,@1982,@1983,@1984,@1985,@1986,@1987,@1988,@1989,@1990)
SET
`1961` = nullif(@1961,''),
`1962` = nullif(@1962,''),
`1963` = nullif(@1963,''),
`1964` = nullif(@1964,''),
`1965` = nullif(@1965,''),
`1966` = nullif(@1966,''),
`1967` = nullif(@1967,''),
`1968` = nullif(@1968,''),
`1969` = nullif(@1969,''),
`1970` = nullif(@1970,''),
`1971` = nullif(@1971,''),
`1972` = nullif(@1972,''),
`1973` = nullif(@1973,''),
`1974` = nullif(@1974,''),
`1975` = nullif(@1975,''),
`1976` = nullif(@1976,''),
`1977` = nullif(@1977,''),
`1978` = nullif(@1978,''),
`1979` = nullif(@1979,''),
`1980` = nullif(@1980,''),
`1981` = nullif(@1981,''),
`1982` = nullif(@1982,''),
`1983` = nullif(@1983,''),
`1984` = nullif(@1984,''),
`1985` = nullif(@1985,''),
`1986` = nullif(@1986,''),
`1987` = nullif(@1987,''),
`1988` = nullif(@1988,''),
`1989` = nullif(@1989,''),
`1990` = nullif(@1990,'')\p;

#########################################################################################################
###Add country code to Forest table
#########################################################################################################

DROP TABLE IF EXISTS `ForestStat_1961_1990_sql`\p;
CREATE TABLE  `ForestStat_1961_1990_sql` like `area_20_ids_MS_raw`.`ForestStat_1961_1990`\p;   #UPDATE
INSERT INTO `ForestStat_1961_1990_sql` SELECT * FROM  `area_20_ids_MS_raw`.`ForestStat_1961_1990`\p;  #UPDATE

#2. Add country_code column
ALTER TABLE `ForestStat_1961_1990_sql` ADD COLUMN `country_code` INTEGER AFTER `country`\p;

#3A. Map from country_data.country_names_2018 table												#UPDATE
UPDATE `ForestStat_1961_1990_sql` corine
LEFT JOIN country_data.country_names_2018 cn ON cn.name = corine.country						#UPDATE
SET corine.country_code =  if(cn.code is null,0,cn.code)\p;

#3B. Delete intentionally dropped country
DELETE FROM `ForestStat_1961_1990_sql` WHERE country_code = -1\p;

#3C. Check to ensure that country are either correctly mapped or intentionally dropped
SELECT IF ((SELECT count(country_code) FROM `ForestStat_1961_1990_sql` WHERE country_code = 0) > 0,@msg,'COUNTRY MAPPING OK')\p;
SELECT DISTINCT country FROM `ForestStat_1961_1990_sql` WHERE country_code = 0\p;

#########################################################################################################
###Add item code/element/element codes
#########################################################################################################

Alter table `ForestStat_1961_1990_sql` add column FAO_item_code INT(5) AFTER item \p;
UPDATE `ForestStat_1961_1990_sql` SET FAO_item_code = 6661\p;

Alter table `ForestStat_1961_1990_sql` add column element varchar(41) AFTER FAO_item_code \p;
UPDATE `ForestStat_1961_1990_sql` SET element = 'Area (1000 Ha)'\p;

Alter table `ForestStat_1961_1990_sql` add column element_ID INT(5) AFTER element \p;
UPDATE `ForestStat_1961_1990_sql` SET element_ID = 5110\p;
#########################################################################################################
###Transpose forest data
#########################################################################################################

	DROP TABLE IF EXISTS `ForestStat_1961_1990_trpse`\p;
	CREATE TABLE `ForestStat_1961_1990_trpse` (
		`country` varchar(41) NOT NULL,
		`country_code` int(10) unsigned NOT NULL,
		`item` varchar(120) NOT NULL,
		`FAO_item_code` int(6) NOT NULL,
		`element` varchar(120) NOT NULL,
		`element_ID` int(6) NOT NULL,
		`year` double NOT NULL,
		`value` double DEFAULT NULL,
	  PRIMARY KEY (`country_code`,`FAO_item_code`, `year`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1\p;

	call `FormatTables`(1961, 1990, 'INSERT INTO `ForestStat_1961_1990_trpse` SELECT Country, country_code, item , FAO_item_code, element,element_ID, $YEAR$ AS year, `$YEAR$` AS value FROM `foreststat_1961_1990_sql` ;') ;


#########################################################################################################
###upload raw forest area data
#########################################################################################################

DROP TABLE IF EXISTS `area_20_ids_MS_raw`.`resourcestat_raw`\p;
CREATE TABLE  `area_20_ids_MS_raw`.`resourcestat_raw`(
  `country_code` int(5) NOT NULL,
  `country` varchar(70) NOT NULL,
  `FAO_item_code` INT(5) NOT NULL,
  `item` varchar(40) NOT NULL,
  `element_ID` varchar(40) NOT NULL,
  `element` varchar(40) NOT NULL,
  `year_code` int(4) NOT NULL,
  `year` int(4) NOT NULL,
  `unit` varchar(15) NOT NULL,
  `value` double DEFAULT NULL,
  `flag` VARCHAR(4) DEFAULT NULL,
PRIMARY KEY  (`country`,`FAO_item_code`, `year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1\p;

LOAD DATA LOCAL INFILE "C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\Inputs_LandUse_E_All_Data_(Normalized).csv"
INTO TABLE `area_20_ids_MS_raw`.`resourcestat_raw` CHARACTER SET  latin1
FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY "\r\n" IGNORE 1 LINES
(country_code, country,FAO_item_code,item,element_ID,element,year_code,year,unit,@value,flag)
SET  `value` = nullif(@value,'')\p;

#########################################################################################################
### Create the sql resource stat data file and transpose it
#########################################################################################################


	DROP TABLE IF EXISTS `area_20_ids_MS_sql`.`resourcestat`\p;					#UPDATE
	CREATE TABLE `area_20_ids_MS_sql`.`resourcestat` (							#UPDATE
		`country` varchar(70) NOT NULL,
		`country_code` smallint(5) NOT NULL,
		`item` varchar(120) NOT NULL,
		`FAO_item_code` smallint(6) NOT NULL,
		`element` varchar(120) NOT NULL,
		`element_ID` int(6) NOT NULL,
		`unit` varchar(15) NOT NULL,
		`year` smallint(4) NOT NULL,
		`value` double DEFAULT NULL,
		`flag` varchar(5) DEFAULT NULL,
	  PRIMARY KEY (`country`,`FAO_item_code`, `year`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1\p;

	INSERT INTO `area_20_ids_MS_sql`.`resourcestat`
	SELECT country, country_code, item, FAO_item_code, element, element_ID, unit, year, value, flag
	FROM `area_20_ids_MS_raw`.`resourcestat_raw` ;

USE `area_20_ids_MS_sql`\p;

#############
#country_code (re)mapping for Resourcestat
#############
#1B.Set country code values to 0 in prep for country mapping
	UPDATE `resourcestat` SET country_code = 0\p;

#2A.Map from country_data.country_names_2018 table							#UPDATE
	UPDATE `resourcestat` resourcestat
	LEFT JOIN country_data.country_names_2018 cn ON cn.name = resourcestat.country		#UPDATE
	SET resourcestat.country_code = if(cn.code is null,0,cn.code)\p;

#2B.Delete intentionally dropped countries
	DELETE FROM `resourcestat` WHERE country_code = -1\p;

#2C.Check to ensure that countries are either correctly mapped or intentionally dropped
	SELECT IF ((SELECT count(country_code) FROM `resourcestat` WHERE country_code = 0) > 0,@msg,'COUNTRY MAPPING OK') ;
	SELECT DISTINCT country FROM `resourcestat` WHERE country_code = 0\p;

#2D.Aggregate where country_code is the same
	DROP TABLE IF EXISTS `rs_intm1`\p;
	CREATE TABLE `rs_intm1` LIKE `resourcestat`\p;
	INSERT INTO `rs_intm1` SELECT country, country_code, item,  FAO_item_code, element, element_ID, unit, year, sum(value), flag FROM `resourcestat` GROUP BY country_code, fao_item_code, element_ID, year ;

#2B.Re-insert back into original table and drop tmp table
	TRUNCATE `resourcestat`\p;
	INSERT INTO `resourcestat` SELECT * FROM `rs_intm1`\p;
	DROP TABLE IF EXISTS `rs_intm1`\p;

ALTER TABLE `resourcestat` drop primary key, add primary key (`country_code`,`FAO_item_code`, `year`)  ;

#2F.Remap country codes to country names to get correct names (for user accessability only)
	UPDATE `resourcestat` rs
	LEFT JOIN country_data.country_names_2018 cn ON cn.code = rs.country_code		#UPDATE
	SET rs.country =  cn.name
	WHERE (cn.primary_name = 1);

#3. Rename "Forest" item to "Forest area" (template matching purposes)
	UPDATE `resourcestat`
	SET item = 'Forest area' WHERE FAO_item_code = 6661;


#4. Fill 'temporary meadows and pastures' data based on average ratio between it and 'arable land'
	INSERT INTO resourcestat
	SELECT country, country_code, item, FAO_item_code, element, element_ID, unit, year, arable_land*ratio, "RFIL"
	FROM (	SELECT country, country_code, year, a.value as `arable_land`
			FROM resourcestat a
			LEFT JOIN (	SELECT country_code, year, value
						FROM resourcestat b
						WHERE fao_item_code = 6633
					  ) b USING(country_code, year)
			WHERE a.fao_item_code = 6621 AND b.value IS NULL
		  ) a
	JOIN (	SELECT country_code, a.item, a.FAO_item_code, a.element, a.element_ID, a.unit, sum(a.value)/sum(b.value) as ratio
			FROM resourcestat a
			JOIN resourcestat b USING(country_code, year)
			WHERE a.fao_item_code = 6633 AND b.fao_item_code = 6621
			GROUP BY country_code
		  ) b USING(country_code) ;

#5. Remove '6633 - Temporary meadows and pastures' from '6620 - Arable land and Permanent crops'
	UPDATE `resourcestat` rs
	RIGHT JOIN (SELECT * FROM area_20_ids_MS_sql.resourcestat where FAO_item_code = 6633) tmp ON rs.country_code = tmp.country_code AND rs.year = tmp.year
	SET rs.value = (rs.value - tmp.value)
	WHERE rs.fao_item_code=6620;

#########################################################################################################
### Delete the zero Forest data for 1961 to 1989
#########################################################################################################

Delete a
from `resourcestat` a
where a.FAO_item_code=6661
and a.year <1990;

#########################################################################################################
### index forest area values based on old forest values available
#########################################################################################################

insert into resourcestat
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	a.element_ID,
	a.unit,
	d.year,
    a.value * b.value / c.value as value,
	'FRST' as flag
FROM
    resourcestat a,
    `area_20_ids_MS_sql`.`foreststat_1961_1990_trpse` b,
    `area_20_ids_MS_sql`.`foreststat_1961_1990_trpse` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE
		a.country_code = b.country_code
        and b.country_code = c.country_code
		and a.FAO_item_code = 6661
        and a.FAO_item_code = b.FAO_item_code
        and b.FAO_item_code = c.FAO_item_code
        and a.year = 1990
        and b.year = d.year
        and c.year = 1990
		and d.year <1990
		 ;


#########################################################################################################
### Delete the zero Forest data Belgium and Luxembourg between 1990 to 1999
#########################################################################################################
Delete a
from `resourcestat` a
where a.country_code in (255,256)
and a.year <2000 \p;

#########################################################################################################
### Split Belgium Lexembourg before 2000
#########################################################################################################


# Check Luxembourg inland water for years 2000 to 2013; if null, insert variable value

DELETE FROM `area_20_ids_MS_sql`.`resourcestat`
WHERE country_code = 256 AND item = 'Inland Water' AND value is null;

INSERT IGNORE INTO `area_20_ids_MS_sql`.`resourcestat`
SELECT 'Luxembourg',256,a.item,a.FAO_item_code,a.element,a.element_ID,a.unit,a.year,b.value-a.value,'BLS'
FROM `area_20_ids_MS_sql`.`resourcestat` a
LEFT JOIN `area_20_ids_MS_sql`.`resourcestat` b ON a.item = b.item
WHERE a.country_code = 255 AND b.country_code = 15 AND a.item = 'Inland Water' and b.year = 1999 and a.year > 1999 ;

# Start split

drop table if exists `area_20_ids_MS_sql`.`Bel_Lux_ratio_tmp` \p;
create table `area_20_ids_MS_sql`.`Bel_Lux_ratio_tmp` (
`fao_item_code` smallint (5) not null,
`bel_perc` double,
`lux_perc` double) \p;

insert into `area_20_ids_MS_sql`.`Bel_Lux_ratio_tmp`
select a.fao_item_code, ifnull(a.value,0)/(ifnull(a.value,0)+ifnull(b.value,0)), ifnull(b.value,0)/(ifnull(a.value,0)+ifnull(b.value,0))
from `resourcestat` a, `resourcestat` b
where a.fao_item_code=b.fao_item_code
and a.country_code=255
and b.country_code=256
and a.year=b.year
and a.year=2000 \p;

insert into `resourcestat`
SELECT
    'Belgium' as country,
    255 as country_code,
    a.item,
	a.FAO_item_code,
	a.element,
	a.element_ID,
	a.unit,
	a.year,
	a.value*b.`bel_perc`,
	'BLS' as flag
	FROM
    `resourcestat` a,
	`area_20_ids_MS_sql`.`Bel_Lux_ratio_tmp` b
	where
    a.fao_item_code=b.fao_item_code
	and a.year<2000
	and a.country_code=15 \p;

	insert into `resourcestat`
SELECT
    'Luxembourg' as country,
    256 as country_code,
    a.item,
	a.FAO_item_code,
	a.element,
	a.element_ID,
	a.unit,
	a.year,
	a.value*b.`lux_perc`,
	'BLS' as flag
	FROM
    `resourcestat` a,
	`area_20_ids_MS_sql`.`Bel_Lux_ratio_tmp` b
	where
    a.fao_item_code=b.fao_item_code
	and a.year<2000
	and a.country_code=15 \p;

# Delete the Bel-Lux values to avoid double counting
DELETE a
from `resourcestat` a
where a.country_code = 15 \p;

#########################################################################################################
### Back fill forest data with linear regression for countries that are missing from foreststat_1961_1990
#########################################################################################################

# 1. Create table of countries that exist in resourcestat but are missing from foreststat_1961_1990

DROP TABLE IF EXISTS `not_in_frstat61_90`\p;
CREATE TABLE `not_in_frstat61_90`(
	country varchar(50),
	country_code int(5)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 \p;

INSERT INTO `not_in_frstat61_90` (country, country_code)
SELECT distinct country, country_code FROM `resourcestat`
WHERE country_code NOT IN (select distinct country_code from `area_20_ids_MS_sql`.`foreststat_1961_1990_trpse`)
AND country_code NOT IN (255, 256) \p;

# 2. Create table where linear regression variables will be calculated based on 1990-2013 resourcestat data

DROP TABLE IF EXISTS `not_in_frstat61_90_regr`;
CREATE TABLE `not_in_frstat61_90_regr` (
	country varchar(50),
	country_code int(5),
	N int(5),
	`meanX` float,
	`sumX` float,
	`sumXX` float,
	`meanY` float,
	`sumY` float,
	`sumYY` float,
	`sumXY` float
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 \p;

INSERT INTO `not_in_frstat61_90_regr`
SELECT
	a.country,
	b.country_code,
	COUNT(b.year),
	AVG(b.year),
	SUM(b.year),
	SUM(b.year*b.year),
	AVG(b.value),
	SUM(b.value),
	SUM(b.value*b.value),
	SUM(b.year*b.value)
FROM `not_in_frstat61_90` a
LEFT JOIN `area_20_ids_MS_sql`.`resourcestat` b on a.country_code = b.country_code
WHERE b.item = 'Forest area'
GROUP BY b.country_code\p;

# 3. Calculate slope and intercept for each country

ALTER TABLE `area_20_ids_MS_sql`.`not_in_frstat61_90_regr`
ADD COLUMN `slope` float after `sumXY` ;

UPDATE 	`not_in_frstat61_90_regr`
SET slope = (N*sumXY - sumX*sumY)/(n*sumXX - sumX*sumX)\p;

ALTER TABLE area_20_ids_MS_sql.`not_in_frstat61_90_regr`
ADD COLUMN `intercept` float after `slope`\p;

UPDATE 	`not_in_frstat61_90_regr`
SET intercept = (meanY-slope*meanX)\p;

# 4. Use slope and intercept to populate forest area data between 1961 and 1989, if country exists in those years

INSERT INTO `resourcestat`
SELECT
	b.country,
	b.country_code,
	a.item,
	a.FAO_item_code,
	a.element,
	a.element_ID,
	a.unit,
	c.year,
	c.year*b.slope+b.intercept as value,
	'FRST' as flag
FROM
	`resourcestat` a,
	`not_in_frstat61_90_regr` b,
	`yearval` c
WHERE
	a.country_code = b.country_code
	and a.item = 'Forest area'
    and a.year = 1990
	and c.year > 1960 and c.year <1990
	\p;

# 5. If any countries have negative forest area, set as zero (as of 1/27/2015, this only applies to Bahrain 1961-1975)

UPDATE `resourcestat`
SET value = 0 where value <0\p;

#########################################################################################################
### Back- and Front-fill all missing years for each country by copying. Only work by country, not by item.
#########################################################################################################

#A. Back-fill countries that are missing data after their start year by copying data from the minimum data year
call BackFillbyCountry(
 'SET @min_year = (SELECT MIN(year) FROM resourcestat WHERE country_code = @country)',
 'INSERT INTO resourcestat
	SELECT country, country_code, item, FAO_item_code, element, element_ID, unit, @min_year-1, value, "CFIL"
	FROM resourcestat
	WHERE country_code = @country AND year = @min_year; '
    ,'country_data.country_matrix_2018') \p;								#UPDATE

#B. Front-fill countries that are missing data before their end year by copying data from the maximum data year
 call FrontFillbyCountry(
 'SET @max_year = (SELECT MAX(year) FROM resourcestat WHERE country_code = @country)',
 'INSERT INTO resourcestat
	SELECT country, country_code, item, FAO_item_code, element, element_ID, unit, @max_year+1, value, "CFIL"
	FROM resourcestat
	WHERE country_code = @country AND year = @max_year; '
    ,'country_data.country_matrix_2018')\p;									#UPDATE

#########################################################################################################
### Update resourcestat sql table with GFN_flag. Call it 'R' if it is any given FAO flag, and keep any
### others create by us up to this point in the GFN_flag column.
#########################################################################################################

alter table resourcestat add column GFN_flag varchar(4) after flag\p;

update resourcestat
set GFN_flag='R'\p;

update resourcestat
set GFN_flag=flag where flag NOT IN ('W','Q','Fm','F','I','E','A')\p;


#########################################################################################################
### Move all countries (except hk_etc) to ult schema
#########################################################################################################

USE `area_20_ids_MS_ult`\p;

DROP TABLE IF EXISTS `resourcestat`\p;
CREATE TABLE `resourcestat` like `area_20_ids_MS_sql`.`resourcestat`\p;
INSERT INTO `resourcestat`
SELECT a.* FROM `area_20_ids_MS_sql`.`resourcestat` a
LEFT JOIN `country_data`.`country_matrix_2018` b							#UPDATE
ON a.country_code = b.country_code
WHERE a.country_code NOT IN (41, 96, 128, 214)
AND a.year >= b.Start_year
AND a.year <= b.End_year \p;


#########################################################################################################
### Move China hk_etc countries to hk_etc schema
#########################################################################################################

USE `area_20_ids_MS_ult_hketc` ;

DROP TABLE IF EXISTS `resourcestat`\p;
CREATE TABLE `resourcestat` like `area_20_ids_MS_sql`.`resourcestat`\p;
INSERT INTO `resourcestat`
SELECT a.* FROM `area_20_ids_MS_sql`.`resourcestat` a
LEFT JOIN `country_data`.`country_matrix_2018` b							#UPDATE
ON a.country_code = b.country_code
WHERE a.country_code IN (41, 96, 128, 214)
AND a.year >= b.Start_year
AND a.year <= b.End_year \p;


###---------------------------------------------------------------###
###---------------------------------------------------------------###
### STEP 3: CORINE -----------------------------------------------###
###---------------------------------------------------------------###
###---------------------------------------------------------------###

USE `area_20_ids_MS_raw`;

#Populate variables for later use
SET @msg = 'ERROR: UNMAPPED COUNTRY';

#########################################################################################################
###upload Corine RAW, 3 different tables for 3 years of data (1990,2000,2006) fix the raw file directory that you are pulling data from
#########################################################################################################

DROP TABLE IF EXISTS `Corine_1990_raw`;
CREATE TABLE  `Corine_1990_raw`(
  `country` varchar(60) NOT NULL,
	`111` float DEFAULT NULL,`112` double DEFAULT NULL,`121` double DEFAULT NULL,`122` double DEFAULT NULL,
	`123` double DEFAULT NULL,`124` double DEFAULT NULL,`131` double DEFAULT NULL,`132` double DEFAULT NULL,
	`133` double DEFAULT NULL,`141` double DEFAULT NULL,`142` double DEFAULT NULL,`211` double DEFAULT NULL,
	`212` double DEFAULT NULL,`213` double DEFAULT NULL,`221` double DEFAULT NULL,`222` double DEFAULT NULL,
	`223` double DEFAULT NULL,`231` double DEFAULT NULL,`241` double DEFAULT NULL,`242` double DEFAULT NULL,
	`243` double DEFAULT NULL,`244` double DEFAULT NULL,`311` double DEFAULT NULL,`312` double DEFAULT NULL,
	`313` double DEFAULT NULL,`321` double DEFAULT NULL,`322` double DEFAULT NULL,`323` double DEFAULT NULL,
	`324` double DEFAULT NULL,`331` double DEFAULT NULL,`332` double DEFAULT NULL,`333` double DEFAULT NULL,
	`334` double DEFAULT NULL,`335` double DEFAULT NULL,`411` double DEFAULT NULL,`412` double DEFAULT NULL,
	`421` double DEFAULT NULL,`422` double DEFAULT NULL,`423` double DEFAULT NULL,`511` double DEFAULT NULL,
	`512` double DEFAULT NULL,`521` double DEFAULT NULL,`522` double DEFAULT NULL,`523` double DEFAULT NULL,
	`No Data` double DEFAULT NULL,
PRIMARY KEY  (`country`)

) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;

LOAD DATA LOCAL INFILE "C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\CorineLand1990_2019.csv"			#UPDATE
INTO TABLE `Corine_1990_raw` CHARACTER SET  latin1
FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY "\r\n" IGNORE 1 LINES
(country,@111,@112,@121,@122,@123,@124,@131,@132,@133,@141,@142,@211,@212,@213,@221,@222,@223,@231,@241,@242,@243,@244,@311,@312,@313,@321,@322,@323,@324,@331,@332,@333,@334,@335,@411,@412,@421,@422,@423,@511,@512,@521,@522,@523,@NoData)

SET
`111` = if(@111='  ',0,@111),
`112` = if(@112='  ',0,@112),
`121` = if(@121='  ',0,@121),
`122` = if(@122='  ',0,@122),
`123` = if(@123='  ',0,@123),
`124` = if(@124='  ',0,@124),
`131` = if(@131='  ',0,@131),
`132` = if(@132='  ',0,@132),
`133` = if(@133='  ',0,@133),
`141` = if(@141='  ',0,@141),
`142` = if(@142='  ',0,@142),
`211` = if(@211='  ',0,@211),
`212` = if(@212='  ',0,@212),
`213` = if(@213='  ',0,@213),
`221` = if(@221='  ',0,@221),
`222` = if(@222='  ',0,@222),
`223` = if(@223='  ',0,@223),
`231` = if(@231='  ',0,@231),
`241` = if(@241='  ',0,@241),
`242` = if(@242='  ',0,@242),
`243` = if(@243='  ',0,@243),
`244` = if(@244='  ',0,@244),
`311` = if(@311='  ',0,@311),
`312` = if(@312='  ',0,@312),
`313` = if(@313='  ',0,@313),
`321` = if(@321='  ',0,@321),
`322` = if(@322='  ',0,@322),
`323` = if(@323='  ',0,@323),
`324` = if(@324='  ',0,@324),
`331` = if(@331='  ',0,@331),
`332` = if(@332='  ',0,@332),
`333` = if(@333='  ',0,@333),
`334` = if(@334='  ',0,@334),
`335` = if(@335='  ',0,@335),
`411` = if(@411='  ',0,@411),
`412` = if(@412='  ',0,@412),
`421` = if(@421='  ',0,@421),
`422` = if(@422='  ',0,@422),
`423` = if(@423='  ',0,@423),
`511` = if(@511='  ',0,@511),
`512` = if(@512='  ',0,@512),
`521` = if(@521='  ',0,@521),
`522` = if(@522='  ',0,@522),
`523` = if(@523='  ',0,@523),
`No data` = if(@NoData='  ',0,@NoData)

;



###########################################################################################################

DROP TABLE IF EXISTS `Corine_2000_raw`;
CREATE TABLE  `Corine_2000_raw`(
  `country` varchar(60) NOT NULL,
	`111` double DEFAULT NULL,`112` double DEFAULT NULL,`121` double DEFAULT NULL,`122` double DEFAULT NULL,
	`123` double DEFAULT NULL,`124` double DEFAULT NULL,`131` double DEFAULT NULL,`132` double DEFAULT NULL,
	`133` double DEFAULT NULL,`141` double DEFAULT NULL,`142` double DEFAULT NULL,`211` double DEFAULT NULL,
	`212` double DEFAULT NULL,`213` double DEFAULT NULL,`221` double DEFAULT NULL,`222` double DEFAULT NULL,
	`223` double DEFAULT NULL,`231` double DEFAULT NULL,`241` double DEFAULT NULL,`242` double DEFAULT NULL,
	`243` double DEFAULT NULL,`244` double DEFAULT NULL,`311` double DEFAULT NULL,`312` double DEFAULT NULL,
	`313` double DEFAULT NULL,`321` double DEFAULT NULL,`322` double DEFAULT NULL,`323` double DEFAULT NULL,
	`324` double DEFAULT NULL,`331` double DEFAULT NULL,`332` double DEFAULT NULL,`333` double DEFAULT NULL,
	`334` double DEFAULT NULL,`335` double DEFAULT NULL,`411` double DEFAULT NULL,`412` double DEFAULT NULL,
	`421` double DEFAULT NULL,`422` double DEFAULT NULL,`423` double DEFAULT NULL,`511` double DEFAULT NULL,
	`512` double DEFAULT NULL,`521` double DEFAULT NULL,`522` double DEFAULT NULL,`523` double DEFAULT NULL,
	`No Data` double DEFAULT NULL,
PRIMARY KEY  (`country`)

) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;

LOAD DATA LOCAL INFILE "C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\CorineLand2000_2019.csv"			#UPDATE
INTO TABLE `Corine_2000_raw` CHARACTER SET  latin1
FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY "\r\n" IGNORE 1 LINES
(country,@111,@112,@121,@122,@123,@124,@131,@132,@133,@141,@142,@211,@212,@213,@221,@222,@223,@231,@241,@242,@243,@244,@311,@312,@313,@321,@322,@323,@324,@331,@332,@333,@334,@335,@411,@412,@421,@422,@423,@511,@512,@521,@522,@523,@NoData)

SET
`111` = if(@111='  ',0,@111),
`112` = if(@112='  ',0,@112),
`121` = if(@121='  ',0,@121),
`122` = if(@122='  ',0,@122),
`123` = if(@123='  ',0,@123),
`124` = if(@124='  ',0,@124),
`131` = if(@131='  ',0,@131),
`132` = if(@132='  ',0,@132),
`133` = if(@133='  ',0,@133),
`141` = if(@141='  ',0,@141),
`142` = if(@142='  ',0,@142),
`211` = if(@211='  ',0,@211),
`212` = if(@212='  ',0,@212),
`213` = if(@213='  ',0,@213),
`221` = if(@221='  ',0,@221),
`222` = if(@222='  ',0,@222),
`223` = if(@223='  ',0,@223),
`231` = if(@231='  ',0,@231),
`241` = if(@241='  ',0,@241),
`242` = if(@242='  ',0,@242),
`243` = if(@243='  ',0,@243),
`244` = if(@244='  ',0,@244),
`311` = if(@311='  ',0,@311),
`312` = if(@312='  ',0,@312),
`313` = if(@313='  ',0,@313),
`321` = if(@321='  ',0,@321),
`322` = if(@322='  ',0,@322),
`323` = if(@323='  ',0,@323),
`324` = if(@324='  ',0,@324),
`331` = if(@331='  ',0,@331),
`332` = if(@332='  ',0,@332),
`333` = if(@333='  ',0,@333),
`334` = if(@334='  ',0,@334),
`335` = if(@335='  ',0,@335),
`411` = if(@411='  ',0,@411),
`412` = if(@412='  ',0,@412),
`421` = if(@421='  ',0,@421),
`422` = if(@422='  ',0,@422),
`423` = if(@423='  ',0,@423),
`511` = if(@511='  ',0,@511),
`512` = if(@512='  ',0,@512),
`521` = if(@521='  ',0,@521),
`522` = if(@522='  ',0,@522),
`523` = if(@523='  ',0,@523),
`No data` = if(@NoData='  ',0,@NoData);



###########################################################################################################

DROP TABLE IF EXISTS `Corine_2006_raw`;
CREATE TABLE  `Corine_2006_raw`(
  `country` varchar(60) NOT NULL,
	`111` double DEFAULT NULL,`112` double DEFAULT NULL,`121` double DEFAULT NULL,`122` double DEFAULT NULL,
	`123` double DEFAULT NULL,`124` double DEFAULT NULL,`131` double DEFAULT NULL,`132` double DEFAULT NULL,
	`133` double DEFAULT NULL,`141` double DEFAULT NULL,`142` double DEFAULT NULL,`211` double DEFAULT NULL,
	`212` double DEFAULT NULL,`213` double DEFAULT NULL,`221` double DEFAULT NULL,`222` double DEFAULT NULL,
	`223` double DEFAULT NULL,`231` double DEFAULT NULL,`241` double DEFAULT NULL,`242` double DEFAULT NULL,
	`243` double DEFAULT NULL,`244` double DEFAULT NULL,`311` double DEFAULT NULL,`312` double DEFAULT NULL,
	`313` double DEFAULT NULL,`321` double DEFAULT NULL,`322` double DEFAULT NULL,`323` double DEFAULT NULL,
	`324` double DEFAULT NULL,`331` double DEFAULT NULL,`332` double DEFAULT NULL,`333` double DEFAULT NULL,
	`334` double DEFAULT NULL,`335` double DEFAULT NULL,`411` double DEFAULT NULL,`412` double DEFAULT NULL,
	`421` double DEFAULT NULL,`422` double DEFAULT NULL,`423` double DEFAULT NULL,`511` double DEFAULT NULL,
	`512` double DEFAULT NULL,`521` double DEFAULT NULL,`522` double DEFAULT NULL,`523` double DEFAULT NULL,
	`No Data` double DEFAULT NULL,
PRIMARY KEY  (`country`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;

LOAD DATA LOCAL INFILE "C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\CorineLand2006_2019.csv"				#UPDATE
INTO TABLE `Corine_2006_raw` CHARACTER SET  latin1
FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY "\r\n" IGNORE 1 LINES
(country,@111,@112,@121,@122,@123,@124,@131,@132,@133,@141,@142,@211,@212,@213,@221,@222,@223,@231,@241,@242,@243,@244,@311,@312,@313,@321,@322,@323,@324,@331,@332,@333,@334,@335,@411,@412,@421,@422,@423,@511,@512,@521,@522,@523,@NoData)

SET
`111` = if(@111='  ',0,@111),
`112` = if(@112='  ',0,@112),
`121` = if(@121='  ',0,@121),
`122` = if(@122='  ',0,@122),
`123` = if(@123='  ',0,@123),
`124` = if(@124='  ',0,@124),
`131` = if(@131='  ',0,@131),
`132` = if(@132='  ',0,@132),
`133` = if(@133='  ',0,@133),
`141` = if(@141='  ',0,@141),
`142` = if(@142='  ',0,@142),
`211` = if(@211='  ',0,@211),
`212` = if(@212='  ',0,@212),
`213` = if(@213='  ',0,@213),
`221` = if(@221='  ',0,@221),
`222` = if(@222='  ',0,@222),
`223` = if(@223='  ',0,@223),
`231` = if(@231='  ',0,@231),
`241` = if(@241='  ',0,@241),
`242` = if(@242='  ',0,@242),
`243` = if(@243='  ',0,@243),
`244` = if(@244='  ',0,@244),
`311` = if(@311='  ',0,@311),
`312` = if(@312='  ',0,@312),
`313` = if(@313='  ',0,@313),
`321` = if(@321='  ',0,@321),
`322` = if(@322='  ',0,@322),
`323` = if(@323='  ',0,@323),
`324` = if(@324='  ',0,@324),
`331` = if(@331='  ',0,@331),
`332` = if(@332='  ',0,@332),
`333` = if(@333='  ',0,@333),
`334` = if(@334='  ',0,@334),
`335` = if(@335='  ',0,@335),
`411` = if(@411='  ',0,@411),
`412` = if(@412='  ',0,@412),
`421` = if(@421='  ',0,@421),
`422` = if(@422='  ',0,@422),
`423` = if(@423='  ',0,@423),
`511` = if(@511='  ',0,@511),
`512` = if(@512='  ',0,@512),
`521` = if(@521='  ',0,@521),
`522` = if(@522='  ',0,@522),
`523` = if(@523='  ',0,@523),
`No data` = if(@NoData='  ',0,@NoData) ;



#########################################################################################################
### Create tables for grouping corine categories to GFN land type 3 different tables for 3 years of data (1990,2000,2006), corine country list
#########################################################################################################
USE `area_20_ids_MS_sql`;

DROP TABLE IF EXISTS `corine_1990_grouped` ;
CREATE TABLE `corine_1990_grouped` (
`country` varchar(50),
`Artificial surfaces` double,
`Arable land + Permanent crops`  double,
`Pastures + Heterogeneous agricultural areas`  double,
`Forests`  double,
`Inland waters`  double) ;

###########################################################################################################
DROP TABLE IF EXISTS `corine_2000_grouped` ;
CREATE TABLE `corine_2000_grouped` (
`country` varchar(50),
`Artificial surfaces` double,
`Arable land + Permanent crops`  double,
`Pastures + Heterogeneous agricultural areas`  double,
`Forests`  double,
`Inland waters`  double);

###########################################################################################################
DROP TABLE IF EXISTS `corine_2006_grouped` ;
CREATE TABLE `corine_2006_grouped` (
`country` varchar(50),
`Artificial surfaces` double,
`Arable land + Permanent crops`  double,
`Pastures + Heterogeneous agricultural areas`  double,
`Forests`  double,
`Inland waters`  double);

#########################################################################################################
### aggregate corine land type into grouped table, 3 different tables for 3 years of data (1990,2000,2006), corine country list
#########################################################################################################

INSERT INTO `corine_1990_grouped` (
SELECT
a.country,
a.`111`+a.`112`+a.`121`+a.`122`+a.`123`+a.`124`+a.`131`+a.`132`+a.`133`+a.`141`+a.`142`,
a.`211`+a.`212`+a.`213`+a.`221`+a.`222`+a.`223`+a.`241`+a.`242`+a.`243`,
a.`231`+a.`321`+a.`322`+a.`323`,
a.`311`+a.`312`+a.`313`+a.`324`,
a.`511` + a.`512`
 FROM `area_20_ids_MS_raw`.`corine_1990_raw` a
);
###########################################################################################################

INSERT INTO `corine_2000_grouped` (
SELECT
b.country,
b.`111`+b.`112`+b.`121`+b.`122`+b.`123`+b.`124`+b.`131`+b.`132`+b.`133`+b.`141`+b.`142`,
b.`211`+b.`212`+b.`213`+b.`221`+b.`222`+b.`223`+b.`241`+b.`242`+b.`243`,
b.`231`+b.`321`+b.`322`+b.`323`,
b.`311`+b.`312`+b.`313`+b.`324`,
b.`511` + b.`512`
 FROM `area_20_ids_MS_raw`.`corine_2000_raw` b
);
###########################################################################################################

INSERT INTO `corine_2006_grouped` (
SELECT
c.country,
c.`111`+c.`112`+c.`121`+c.`122`+c.`123`+c.`124`+c.`131`+c.`132`+c.`133`+c.`141`+c.`142`,
c.`211`+c.`212`+c.`213`+c.`221`+c.`222`+c.`223`+c.`241`+c.`242`+c.`243`,
c.`231`+c.`321`+c.`322`+c.`323`,
c.`311`+c.`312`+c.`313`+c.`324`,
c.`511` + c.`512`
FROM `area_20_ids_MS_raw`.`corine_2006_raw` c
);
#########################################################################################################
### transpose years as column to year as rows corine, 3 different tables for 3 years of data (1990,2000,2006), corine country list
#########################################################################################################
drop table if exists Corine_1990_trpse  ;
Create table Corine_1990_trpse(
country varchar(40),#
item varchar(60),
`Year` int(5),
Value double
);
call Format_Corine_Tables(1, 5, 'INSERT INTO `Corine_1990_trpse` SELECT country, ''$YEAR$'' AS item,"1990" AS `year` ,`$YEAR$` AS value FROM `corine_1990_grouped` ;') ;

###########################################################################################################
drop table if exists Corine_2000_trpse  ;
Create table Corine_2000_trpse(
country varchar(40),#
item varchar(60),
`Year` int(5),
Value double
);
call Format_Corine_Tables(1, 5, 'INSERT INTO `Corine_2000_trpse` SELECT country, ''$YEAR$'' AS item,"2000" AS `year` ,`$YEAR$` AS value FROM `corine_2000_grouped` ;') ;

###########################################################################################################
drop table if exists Corine_2006_trpse ;
Create table Corine_2006_trpse(
country varchar(255),#
item varchar(60),
`Year` int(5),
Value double
);
call Format_Corine_Tables(1, 5, 'INSERT INTO `Corine_2006_trpse` SELECT country, ''$YEAR$'' AS item,"2006" AS `year` ,`$YEAR$` AS value FROM `corine_2006_grouped` ;') ;

#########################################################################################################
###Add country code to corine table, 3 different tables for 3 years of data (1990,2000,2006), corine country list
#########################################################################################################

#2. Add country_code  column
ALTER TABLE `corine_1990_trpse` ADD COLUMN `country_code` INTEGER AFTER `country` ;

#3A. Map from country_data.country_names_2018 table											#UPDATE
UPDATE `corine_1990_trpse` corine
LEFT JOIN country_data.country_names_2018 cn ON cn.name = corine.country					#UPDATE
SET corine.country_code  =  if(cn.code is null,0,cn.code) ;

#3B. Delete intentionally dropped countries
DELETE FROM `corine_1990_trpse` WHERE country_code  = -1 ;

#3C. Check to ensure that countries are either correctly mapped or intentionally dropped
SELECT IF ((SELECT count(country_code ) FROM `corine_1990_trpse` WHERE country_code  = 0) > 0,@msg,'COUNTRY MAPPING OK') ;
SELECT DISTINCT country FROM `corine_1990_trpse` WHERE country_code  = 0;

###########################################################################################################

#2. Add country_code  column
ALTER TABLE `corine_2000_trpse` ADD COLUMN `country_code` INTEGER AFTER `country` ;

#3A. Map from country_data.country_names_2018 table											#UPDATE
UPDATE `corine_2000_trpse` corine
LEFT JOIN country_data.country_names_2018 cn ON cn.name = corine.country					#UPDATE
SET corine.country_code  =  if(cn.code is null,0,cn.code) ;

#3B. Delete intentionally dropped countries
DELETE FROM `corine_2000_trpse` WHERE country_code  = -1;

#3C. Check to ensure that countries are either correctly mapped or intentionally dropped
SELECT IF ((SELECT count(country_code ) FROM `corine_2000_trpse` WHERE country_code  = 0) > 0,@msg,'COUNTRY MAPPING OK') ;
SELECT DISTINCT country FROM `corine_2000_trpse` WHERE country_code  = 0;

###########################################################################################################

#2. Add country_code  column
ALTER TABLE `corine_2006_trpse` ADD COLUMN `country_code` INTEGER AFTER `country` ;

#3A. Map from country_data.country_names_2018 table						#UPDATE
UPDATE `corine_2006_trpse` corine
LEFT JOIN country_data.country_names_2018 cn ON cn.name = corine.country					#UPDATE
SET corine.country_code  =  if(cn.code is null,0,cn.code);

#3B. Delete intentionally dropped countries
DELETE FROM `corine_2006_trpse` WHERE country_code  = -1;

#3C. Check to ensure that countries are either correctly mapped or intentionally dropped
SELECT IF ((SELECT count(country_code ) FROM `corine_2006_trpse` WHERE country_code  = 0) > 0,@msg,'COUNTRY MAPPING OK') ;
SELECT DISTINCT country FROM `corine_2006_trpse` WHERE country_code  = 0;

#########################################################################################################
UPDATE `corine_1990_trpse` corine
LEFT JOIN country_data.country_names_2018 cn ON cn.code = corine.country_code 				#UPDATE
SET corine.country =  cn.name
WHERE (cn.primary_name = 1);
#########################################################################################################
UPDATE `corine_2000_trpse` corine
LEFT JOIN country_data.country_names_2018 cn ON cn.code = corine.country_code 				#UPDATE
SET corine.country =  cn.name
WHERE (cn.primary_name = 1);
#########################################################################################################
UPDATE `corine_2006_trpse` corine
LEFT JOIN country_data.country_names_2018 cn ON cn.code = corine.country_code 				#UPDATE
SET corine.country =  cn.name
WHERE (cn.primary_name = 1);

#########################################################################################################
### Identify the outliers in corine data, using 2000 as reference to evaluate 1990 and 2006 data
### storing country_code  and item in a seperate table to use as reference. fixed change percentage below 60% or above 140%
#########################################################################################################
 DROP TABLE IF EXISTS `1990_outlier_values` ;
 create  table `1990_outlier_values` (
`country_code` int(10) unsigned NOT NULL,
`item` varchar(50)
)  ;
 DROP TABLE IF EXISTS `2006_outlier_values` ;
 create  table `2006_outlier_values` (
`country_code` int(10) unsigned NOT NULL,
`item` varchar(50)
)  ;

 insert into `1990_outlier_values`
(select a.country_code  ,a.item
FROM `corine_1990_trpse` a
left join `corine_2000_trpse` b
on a.country_code  =b.country_code
and a.item =b.item where (a.value/b.value)*100 <60
or (a.value/b.value)*100 >140) ;

 insert into `2006_outlier_values`
(select a.country_code  ,a.item
FROM `corine_2000_trpse` a
left join `corine_2006_trpse` b
on a.country_code  =b.country_code
and a.item =b.item where (a.value/b.value)*100 <60
or (a.value/b.value)*100 >140) ;

#########################################################################################################
### Create final table for corine data and add 1990,2000,2006 data
#########################################################################################################

DROP TABLE IF EXISTS `Corine_penult` \p;
CREATE TABLE  `Corine_penult`(
`country` varchar(40),
`country_code` int(5),
item varchar(60),
`Year` int(5),
`Value` double ,
`GFN_flag` varchar(4),
PRIMARY KEY  (`country_code`,item,`Year`)
) \p ;

insert into `Corine_penult`( select *,'R' as GFN_flag from  `area_20_ids_MS_sql`.`corine_1990_trpse`) \p;
insert into `Corine_penult`( select *,'R' as GFN_flag from  `area_20_ids_MS_sql`.`corine_2000_trpse`) \p;
insert into `Corine_penult`( select *,'R' as GFN_flag from  `area_20_ids_MS_sql`.`corine_2006_trpse`) \p;

#########################################################################################################
### Assign FAO Item code to CORINE land classes, sicne resourcestata does not have Artificial surfaces we assige 9999 to this class in CORINE
#########################################################################################################

Alter table `Corine_penult` add column FAO_item_code INT(5) AFTER ITEM \p;

UPDATE `Corine_penult` SET FAO_item_code = IF
(ITEM ='Arable land + Permanent crops', 6620,
IF (ITEM ='Artificial surfaces',9999,
IF (ITEM='Forests',6661,
IF (ITEM='Inland waters',6680,
IF (ITEM ='Pastures + Heterogeneous agricultural areas',6655,-1))))) \p;
#########################################################################################################
### Delete the wrong expection values from CORINE
#########################################################################################################
Delete a
from `Corine_penult` a left join `area_20_ids_MS_sql`.`1990_outlier_values` b
on a.country_code  =b.country_code  and a.item=b.item
where a.country_code  =b.country_code  and a.item=b.item and year =1990 \p;

Delete a
from `Corine_penult` a left join `area_20_ids_MS_sql`.`2006_outlier_values`b
on a.country_code  =b.country_code  and a.item=b.item
where a.country_code  =b.country_code  and a.item=b.item and year =2006 \p;

# Delete Bosnia and Herzegovina and Macedonia TFYR (because they did not exist in 1990)? Yes

Delete from `Corine_penult`
	where country_code  in(80,154)
	and year = 1990 \p;

#########################################################################################################
### insert new values for exception data 1990
#########################################################################################################
insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
    1990,
    a.value * b.value / c.value,
    'OUT' as GFN_flag
FROM
    `Corine_penult` a,
    (SELECT
		*
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		fao_item_code IN (6620 , 6661, 6680)
	UNION SELECT
		country,
		country_code,
		item,
		6655,
		element,
		element_ID,
		unit,
		year,
		SUM(value),
		flag,
        GFN_flag
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		FAO_item_code IN (6655 , 6633)
	GROUP BY country_code, year) b,
    (SELECT
		*
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		fao_item_code IN (6620 , 6661, 6680)
	UNION SELECT
		country,
		country_code,
		item,
		6655,
		element,
		element_ID,
		unit,
		year,
		SUM(value),
		flag,
        GFN_flag
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		FAO_item_code IN (6655 , 6633)
	GROUP BY country_code, year) c,
	`area_20_ids_MS_sql`.`1990_outlier_values` d
WHERE
   		a.country_code = d.country_code
		and a.item =d.item
        and a.country_code = b.country_code
        and a.country_code = c.country_code
        and a.FAO_item_code = b.FAO_item_code
        and b.FAO_item_code = c.FAO_item_code
        and a.year = 2000
        and b.year = 1990
        and c.year = 2000 \p;

insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
    1990,
    a.value * b.value / c.value,
    'OUT' as GFN_flag
FROM
    `Corine_penult` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
    `faostat_18_v1_ult`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`1990_outlier_values` d

WHERE
		a.country_code = d.country_code
		and a.item =d.item
		and	a.FAO_item_code = 9999
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
        and a.country_code = b.country_code
        and a.country_code = c.country_code
        AND a.year = 2000
        and b.year = 1990
        and c.year = 2000 \p;

#########################################################################################################
### insert new values for exception data 2006
#########################################################################################################

insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
    2006,
    a.value * b.value / c.value,
    'OUT' as GFN_flag
FROM
    `Corine_penult` a,
    (SELECT
		*
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		fao_item_code IN (6620 , 6661, 6680)
	UNION SELECT
		country,
		country_code,
		item,
		6655,
		element,
		element_ID,
		unit,
		year,
		SUM(value),
		flag,
        GFN_flag
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		FAO_item_code IN (6655 , 6633)
	GROUP BY country_code, year) b,
    (SELECT
		*
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		fao_item_code IN (6620 , 6661, 6680)
	UNION SELECT
		country,
		country_code,
		item,
		6655,
		element,
		element_ID,
		unit,
		year,
		SUM(value),
		flag,
        GFN_flag
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		FAO_item_code IN (6655 , 6633)
	GROUP BY country_code, year) c,
	`area_20_ids_MS_sql`.`2006_outlier_values` d
WHERE
		a.country_code = d.country_code
		and a.item =d.item
        and a.country_code = b.country_code
        and a.country_code = c.country_code
        and a.FAO_item_code = b.FAO_item_code
        and b.FAO_item_code = c.FAO_item_code
        AND a.year = 2000
        and b.year = 2006
        and c.year = 2000  \p;

insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
    2006,
    a.value * b.value / c.value,
    'OUT' as GFN_flag
FROM
    `Corine_penult` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
    `faostat_18_v1_ult`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`2006_outlier_values` d
WHERE
		a.country_code = d.country_code
		and a.item =d.item
		and	a.FAO_item_code = 9999
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
        and a.country_code = b.country_code
        and a.country_code = c.country_code
        AND a.year = 2000
        and b.year = 2006
        and c.year = 2000 \p;

#########################################################################################################
### insert new values for exception data 1992
#########################################################################################################

insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
    1992,
    a.value * b.value / c.value,
    'OUT' as GFN_flag
FROM
    `Corine_penult` a,
    (SELECT
		*
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		fao_item_code IN (6620 , 6661, 6680)
	UNION SELECT
		country,
		country_code,
		item,
		6655,
		element,
		element_ID,
		unit,
		year,
		SUM(value),
		flag,
        GFN_flag
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		FAO_item_code IN (6655 , 6633)
	GROUP BY country_code, year) b,
    (SELECT
		*
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		fao_item_code IN (6620 , 6661, 6680)
	UNION SELECT
		country,
		country_code,
		item,
		6655,
		element,
		element_ID,
		unit,
		year,
		SUM(value),
		flag,
        GFN_flag
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		FAO_item_code IN (6655 , 6633)
	GROUP BY country_code, year) c
WHERE
    a.country_code in (80,154)
		and a.FAO_item_code in (6620,6655,6661,6680)
        and a.country_code = b.country_code
        and a.country_code = c.country_code
        and a.FAO_item_code = b.FAO_item_code
        and b.FAO_item_code = c.FAO_item_code
        AND a.year = 2000
        and b.year = 1992
        and c.year = 2000 \p;

insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
    1992,
    a.value * b.value / c.value,
    'OUT' as GFN_flag
FROM
    `Corine_penult` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
    `faostat_18_v1_ult`.`popstat_ult` c

WHERE
    a.country_code in (80,154)
		and	a.FAO_item_code = 9999
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
        and a.country_code = b.country_code
        and a.country_code = c.country_code
        AND a.year = 2000
        and b.year = 1992
        and c.year = 2000 \p;


#########################################################################################################
### calculate for the value between years 1990 and 2000
#########################################################################################################

insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
    c.year,
    a.value + ((b.value - a.value) * (c.year-a.year) / 10),
    'FIL'
FROM
    `Corine_penult` a,
    `Corine_penult` b,
	`area_20_ids_MS_sql`.`yearval` c
WHERE
		a.country_code not in (80,154)
		and a.country_code = b.country_code
        AND a.year = 1990
        AND b.year = 2000
        AND a.item = b.item
		and c.year>1990
		and c.year<2000 \p
		 ;

#########################################################################################################
### calculate for the value between years 1992 and 2000 for Bosnia and Macedonia
#########################################################################################################

insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
    c.year,
    a.value + ((b.value - a.value) * (c.year-a.year) / 8),
    'FIL' as GFN_flag
FROM
    `Corine_penult` a,
    `Corine_penult` b,
	`area_20_ids_MS_sql`.`yearval` c
WHERE
		a.country_code  in (80,154)
		and a.country_code = b.country_code
        AND a.year = 1992
        AND b.year = 2000
        AND a.item = b.item
		and c.year>1992
		and c.year<2000	\p;


#########################################################################################################
### calculate for the value between years 2000 and 2006
#########################################################################################################

insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
    c.year,
    a.value + ((b.value - a.value) * (c.year-a.year) / 6),
    'FIL' as GFN_flag
FROM
    `Corine_penult` a,
    `Corine_penult` b,
	`area_20_ids_MS_sql`.`yearval` c
WHERE
    a.country_code = b.country_code
        AND a.year = 2000
        AND b.year = 2006
        AND a.item = b.item
		and c.year>2000
		and c.year<2006	\p
		 ;


#########################################################################################################
### calculate for the values between years 1961 and 1990 all land type but artificial surfaces
#########################################################################################################
insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	d.year,
	a.value * b.value/c.value,
    'FIL' as GFN_flag
FROM
    `Corine_penult` a,
    (SELECT
		*
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		fao_item_code IN (6620 , 6661, 6680)
	UNION SELECT
		country,
		country_code,
		item,
		6655,
		element,
		element_ID,
		unit,
		year,
		SUM(value),
		flag,
        GFN_flag
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		FAO_item_code IN (6655 , 6633)
	GROUP BY country_code , year) b,
	(SELECT
		*
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		fao_item_code IN (6620 , 6661, 6680)
	UNION SELECT
		country,
		country_code,
		item,
		6655,
		element,
		element_ID,
		unit,
		year,
		SUM(value),
		flag,
        GFN_flag
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		FAO_item_code IN (6655 , 6633)
	GROUP BY country_code , year) c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE
	a.country_code  not in (80,154)
    and  a.country_code = b.country_code
	and  a.country_code = c.country_code
        and a.year = 1990
		and c.year = 1990
		and a.FAO_item_code in (6620,6655,6661,6680)
        and a.FAO_item_code = b.FAO_item_code
        and b.FAO_item_code = c.FAO_item_code
		and b.year = d.year
		and d.year>1960 and d.year<1990	\p
		 ;


#########################################################################################################
### calculate for the value between years 1961 and 1990 for artificial surfaces
#########################################################################################################
insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	d.year,
    a.value * b.value/c.value,
    'FIL' as GFN_flag
FROM
    `Corine_penult` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
	`faostat_18_v1_ult`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE
	a.country_code  not in (80,154)
    and  a.country_code = b.country_code
	and  a.country_code = c.country_code
        AND a.year = 1990
		and c.year = 1990
        AND a.item = 'Artificial surfaces'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>1960
		and d.year<1990	\p
		 ;


#########################################################################################################
### calculate for the valueS between years 2006 and 2014 all land type but artificial surfaces
#########################################################################################################
insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	d.year,
	a.value * b.value/c.value,
    'FIL' as GFN_flag
FROM
    `Corine_penult` a,
    (SELECT
		*
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		fao_item_code IN (6620 , 6661, 6680)
	UNION SELECT
		country,
		country_code,
		item,
		6655,
		element,
		element_ID,
		unit,
		year,
		SUM(value),
		flag,
        GFN_flag
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		FAO_item_code IN (6655 , 6633)
	GROUP BY country_code, year) b,
	(SELECT
		*
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		fao_item_code IN (6620 , 6661, 6680)
	UNION SELECT
		country,
		country_code,
		item,
		6655,
		element,
		element_ID,
		unit,
		year,
		SUM(value),
		flag,
        GFN_flag
	FROM
		area_20_ids_MS_ult.resourcestat
	WHERE
		FAO_item_code IN (6655 , 6633)
	GROUP BY country_code, year) c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE

    a.country_code = b.country_code
	and  a.country_code = c.country_code
        and a.year = 2006
		and c.year = 2006
		and a.FAO_item_code in (6620,6655,6661,6680)
        and a.country_code = b.country_code
        and a.country_code = c.country_code
        and a.FAO_item_code = b.FAO_item_code
        and b.FAO_item_code = c.FAO_item_code
		and b.year = d.year
		and d.year>2006
		and d.year<=@default_end_year \p
		 ;


#########################################################################################################
### calculate for the value between years 2006 and 2013 for artificial surfaces
#########################################################################################################
insert into `Corine_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	d.year,
    a.value * b.value/c.value,
    'FIL' as GFN_flag
FROM
    `Corine_penult` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
	`faostat_18_v1_ult`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE

     a.country_code = b.country_code
	and  a.country_code = c.country_code
        AND a.year = 2006
		and c.year = 2006
        AND a.item = 'Artificial surfaces'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>2006
		and d.year<=@default_end_year \p
		 ;


################################################
#Divide by 1000 to transform into 1000Ha
################################################
update `Corine_penult` set `value` = `value`/1000  \p;

#########################################################################################################
### Move all countries (except hk_etc) to ult schema
#########################################################################################################

USE `area_20_ids_MS_ult`  \p;

DROP TABLE IF EXISTS `corine`  \p;
CREATE TABLE `corine` like `area_20_ids_MS_sql`.`corine_penult`  \p;
INSERT INTO `corine`
SELECT a.* FROM `area_20_ids_MS_sql`.`corine_penult` a
LEFT JOIN `country_data`.`country_matrix_2018` b										#UPDATE
ON a.country_code = b.country_code
WHERE a.country_code NOT IN (41, 96, 128, 214)
AND a.year >= b.Start_year
AND a.year <= b.End_year  \p;


#########################################################################################################
### Move China hk_etc countries to hk_etc schema
#########################################################################################################

USE `area_20_ids_MS_ult_hketc`  \p;

DROP TABLE IF EXISTS `corine`  \p;
CREATE TABLE `corine` like `area_20_ids_MS_sql`.`corine_penult`  \p;
INSERT INTO `corine`
SELECT a.* FROM `area_20_ids_MS_sql`.`corine_penult` a
LEFT JOIN `country_data`.`country_matrix_2018` b									#UPDATE
ON a.country_code = b.country_code
WHERE a.country_code IN (41, 96, 128, 214)
AND a.year >= b.Start_year
AND a.year <= b.End_year  \p;

# As of 5/29/18 we became aware that corine estimates for 'Arable Land and Permanent Crops' was a large overestimate. Removing this
# results in a more reasonable estimate from FAO.
delete from `corine` where country='Slovenia' and FAO_item_code=6620 \p;

###---------------------------------------------------------------###
###---------------------------------------------------------------###
### STEP 4: GAEZ -------------------------------------------------###
###---------------------------------------------------------------###
###---------------------------------------------------------------###


USE `area_20_ids_MS_raw`  \p;

#Populate variables for later use
SET @msg = 'ERROR: UNMAPPED COUNTRY' \p;

#########################################################################################################
### upload GAEZ land cover values
### NOTE: THE SCRIPT SEEMS TO CREATE AN ELEMENT OF 'AREA' (INSTEAD OF 'AREA (1000 HA)') JUST FOR CHINA, 351 AND THREE OF THE FOUR HK_ETC TERRITORIES. SHOULD BE FIXED AT SOME POINT.
#########################################################################################################
DROP TABLE IF EXISTS `GAEZ_raw` \p;
CREATE TABLE  `GAEZ_raw`(
	`ctr` int(10),
	`reg` int(5),
  `country` varchar(41) NOT NULL,
   `TOTAL` float default null,`VS` float default null,
   `S` float default null,`VS_S` float default null,
   `MS` float default null,`VS_MS` float default null,
   `MS2` float default null,`VS_MS2` float default null,
   `NAG` float default null,`NG` float default null,

PRIMARY KEY  (`country`)

) ENGINE=MyISAM DEFAULT CHARSET=latin1  \p;

LOAD DATA LOCAL INFILE "C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\GAEZ_raw_19.csv"			#UPDATE
INTO TABLE `GAEZ_raw` CHARACTER SET  latin1
FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY "\r\n" IGNORE 1 LINES   \p;

USE `area_20_ids_MS_sql` \p;

DROP TABLE IF EXISTS `GAEZ`  \p;
CREATE TABLE  `GAEZ`(
country varchar(40),
item varchar (40),
FAO_item_code int(10),
element varchar (40),
year int(5),
Value FLOAT ,
PRIMARY KEY  (`country`,`year`)
)  \p;

insert into `GAEZ`
SELECT
country,
'Settlement and infrastructure' as item,
'6661' as FAO_item_code,
'Area (1000 Ha)' as element,
'2000' as year,
`NAG` as value
from
`area_20_ids_MS_raw`.`GAEZ_raw`  \p;

#########################################################################################################
###Add World data
#########################################################################################################

INSERT INTO `GAEZ`
SELECT
'World' as country,
'Settlement and infrastructure' as item,
'6661' as FAO_item_code,
'Area (1000 Ha)' as element,
'2000' as year,
sum(value) as value
FROM
`GAEZ`
WHERE year = 2000  \p;

#########################################################################################################
###Add country code to GAEZ data
#########################################################################################################

#1. Correct China and Sudan name strings
UPDATE `GAEZ` SET country = 'China, mainland' WHERE country = 'China' \p;
UPDATE `GAEZ` SET country = 'Sudan (former)' WHERE country = 'Sudan' \p;

#2. Add country_code column
ALTER TABLE `GAEZ` ADD COLUMN `country_code` INTEGER AFTER `country`  \p;

#3A. Map from country_data.country_names_2018 table										#UPDATE
UPDATE `GAEZ` corine
LEFT JOIN country_data.country_names_2018 cn ON cn.name = corine.country				#UPDATE
SET corine.country_code =  if(cn.code is null,0,cn.code)  \p;

#3B. Delete intentionally dropped countries
DELETE FROM `GAEZ` WHERE country_code = -1 \p;

#3C. Check to ensure that countries are either correctly mapped or intentionally dropped
SELECT IF ((SELECT count(country_code) FROM `GAEZ` WHERE country_code = 0) > 0,@msg,'COUNTRY MAPPING OK')  \p;
SELECT DISTINCT country FROM `GAEZ` WHERE country_code = 0 \p;


#########################################################################################################
### calculate for the value between years 1961 and 2000 for artificial surfaces
#########################################################################################################

insert into `GAEZ`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	d.year,
    a.value * b.value/c.value
FROM
    `GAEZ` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
	`faostat_18_v1_ult`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE

    a.country_code = b.country_code
	and  a.country_code = c.country_code
        AND a.year = 2000
		and c.year = 2000
        AND a.FAO_item_code ='6661'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>1960
		and d.year<2000
		 \p;


### Repetition for China Mainland

insert into `GAEZ`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	d.year,
    a.value * b.value/c.value
FROM
    `GAEZ` a,
    `faostat_18_v1_ult_hketc`.`popstat_ult` b,
	`faostat_18_v1_ult_hketc`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE

    a.country_code = b.country_code
	and  a.country_code = c.country_code
        AND a.year = 2000
		and c.year = 2000
        AND a.FAO_item_code ='6661'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>1960
		and d.year<2000
		  \p;


#########################################################################################################
### 5 - calculate for the value between years 2001 and 2011 for artificial surfaces
#########################################################################################################

INSERT INTO `GAEZ`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	d.year,
    a.value * b.value/c.value
FROM
    `GAEZ` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
	`faostat_18_v1_ult`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE
a.country_code = b.country_code
	and  a.country_code = c.country_code
        AND a.year = 2000
		and c.year = 2000
        AND a.FAO_item_code ='6661'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>2000
		and d.year<=@default_end_year
		 \p;

### Repetition for mainland china

INSERT INTO `GAEZ`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	d.year,
    a.value * b.value/c.value
FROM
    `GAEZ` a,
    `faostat_18_v1_ult_hketc`.`popstat_ult` b,
	`faostat_18_v1_ult_hketc`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE

		a.country_code = b.country_code
		and  a.country_code = c.country_code
        AND a.year = 2000
		and c.year = 2000
        AND a.FAO_item_code ='6661'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>2000
		and d.year<=@default_end_year
		 \p;


#########################################################################################################
###Group and aggregate countries with matching codes
#########################################################################################################

#4A. Create Penult Table

DROP TABLE IF EXISTS `GAEZ_penult`  \p;
CREATE TABLE  `GAEZ_penult`(
country varchar(40),
country_code int(5),
item varchar (40),
FAO_item_code int(10),
element varchar (40),
year int(5),
Value FLOAT ,
PRIMARY KEY  (`country`,`year`)
)  \p;

#4B. Fill Ult table with data, aggregate by country code

INSERT INTO `GAEZ_penult`
SELECT country, country_code, item, FAO_item_code, element, year, SUM(value) from `area_20_ids_MS_sql`.`GAEZ`
Group by country_code, FAO_item_code, year \p;

#4C. Replace country name strings with primary names

UPDATE `GAEZ_penult` g
LEFT JOIN country_data.country_names_2018 cn ON cn.code = g.country_code and cn.primary_name = 1		#UPDATE
SET g.country =  cn.name \p;

#########################################################################################################
### 6 - Add Ethiopia PDR data and extrapolate back to 1961
#########################################################################################################

#6A. Add 'Ethiopia PDR', 1992 area = year 2000 sum from 'Eritrea' and 'Ethiopia'

INSERT INTO `GAEZ_penult`
SELECT
'Ethiopia PDR' as country,
62 as country_code,
'Settlement and infrastructure' as item,
'6661' as FAO_item_code,
'Area (1000 Ha)' as element,
'1992' as year,
sum(value) as value
FROM
`GAEZ_penult`
WHERE year = 2000 and country_code in (178, 238)  \p;

#6B. Extrapolate Ethiopia PDR built area backward from 1992 based on population

INSERT INTO `GAEZ_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	d.year,
    a.value * b.value/c.value
FROM
    `GAEZ_penult` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
	`faostat_18_v1_ult`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE

a.country_code = b.country_code
	and  a.country_code = c.country_code
		and a.country_code = 62
        AND a.year = 1992
		and c.year = 1992
        AND a.FAO_item_code ='6661'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>1960
		and d.year<1992
		  \p;


#########################################################################################################
### 7 - Add USSR data and extrapolate back to 1961
#########################################################################################################


#7A. Add 'USSR', 1991 area = year 2000 sum from Armenia,Azerbaijan,Estonia,Georgia,Kazakhstan,Kyrgyzstan,Latvia,Lithuania,Republic of Moldova,Russian Federation,Tajikistan,Turkmenistan,Ukraine,Uzbekistan


INSERT INTO `GAEZ_penult`
SELECT
'USSR' as country,
228 as country_code,
'Settlement and infrastructure' as item,
'6661' as FAO_item_code,
'Area (1000 Ha)' as element,
'1991' as year,
sum(value) as value
FROM
`GAEZ_penult`
WHERE year = 2000 and country_code in (185, 230, 146, 73, 1, 52, 108, 235, 213, 113, 208, 63, 126, 119)   \p;

#7B. Extrapolate USSR built area backward from 1991 based on population


INSERT INTO `GAEZ_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	d.year,
    a.value * b.value/c.value
FROM
    `GAEZ_penult` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
	`faostat_18_v1_ult`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE

	a.country_code = b.country_code
		and  a.country_code = c.country_code
		AND a.country_code = 228
        AND a.year = 1991
		and c.year = 1991
        AND a.FAO_item_code ='6661'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>1960  and d.year<1991
		  \p;

#########################################################################################################
### 8 - Split Sudan (former) (206) into Sudan (276) and South Sudan (277), by population, in 2011
#########################################################################################################

INSERT INTO `GAEZ_penult`
SELECT
	'Sudan' as country,
	276 as country_code,
	a.item,
	a.FAO_item_code,
	a.element,
	c.year,
	a.value * c.value/b.value
FROM `GAEZ_penult` a
LEFT JOIN `faostat_18_v1_ult`.`popstat_ult` b ON a.country_code = b.country_code AND a.year = b.year
LEFT JOIN `faostat_18_v1_ult`.`popstat_ult` c ON c.element_ID = b.element_ID
WHERE a.country_code = 206	AND a.year = (@Sudan_start_year - 1)
  AND c.country_code = 276 AND c.year >= @Sudan_start_year
  AND c.element_ID = 511
  AND c.year <= @Sudan_end_year\p;

# South Sudan

INSERT INTO `GAEZ_penult`
SELECT
	'South Sudan' as country,
	277 as country_code,
	a.item,
	a.FAO_item_code,
	a.element,
	c.year,
	a.value * c.value/b.value
FROM `GAEZ_penult` a
LEFT JOIN `faostat_18_v1_ult`.`popstat_ult` b ON a.country_code = b.country_code AND a.year = b.year
LEFT JOIN `faostat_18_v1_ult`.`popstat_ult` c ON c.element_ID = b.element_ID
WHERE a.country_code = 206 AND a.year = (@South_Sudan_start_year - 1)
  AND c.country_code = 277 AND c.year >= @South_Sudan_start_year
  AND c.element_ID = 511
  AND c.year <= @South_Sudan_end_year\p;


#########################################################################################################
### 9 - Add values for disaggregated china
#########################################################################################################


#9A. Create table with built-area ratios

DROP TABLE IF EXISTS `China_splits` \p;
CREATE TABLE `China_splits`(
country varchar(40),
country_code int(5),
Value FLOAT,
PRIMARY KEY  (`country`)
)  \p;

Insert into `China_splits` (country, country_code, value)
VALUES
('China Hong Kong SAR', 96, 0.22),
('China, Macao SAR', 128, 0.77),
('Taiwan', 214, 0.07) \p;


#9C. Calculate built area for HK, Macao, and Taiwan based on ratios in china_splits and land area in resourcsetat

INSERT INTO `GAEZ_penult`
SELECT
	a.country, a.country_code, 'Settlement and infrastructure', 6661, a.element, a.year, a.value*b.value
FROM
	`area_20_ids_MS_ult_hketc`.`resourcestat` a,
	`area_20_ids_MS_sql`.`China_splits` b
WHERE
	a.country_code=b.country_code
	and a.FAO_item_code = 6601  \p;

#9D. Calcualte built area for China, mainland by subtracting the calculated built areas of HK, Macao, and Taiwan from the GAEZ built area for china aggregate

INSERT INTO `GAEZ_penult`
SELECT
	'China' as country,
	351 as country_code,
	item,
	FAO_item_code,
	element,
	year,
	sum(value)
FROM `GAEZ_penult`
WHERE country_code in (41, 96, 128, 214)
GROUP BY YEAR  \p;


#########################################################################################################
### Copy GAEZ_penult to ult schema, filtering by country_matrix_2018 and hk_etc countries
#########################################################################################################

USE `area_20_ids_MS_ult` \p;

DROP TABLE IF EXISTS `GAEZ`  \p;
CREATE TABLE `GAEZ` like `area_20_ids_MS_sql`.`GAEZ_penult` \p;
INSERT INTO `GAEZ`
SELECT a.* FROM `area_20_ids_MS_sql`.`GAEZ_penult` a
LEFT JOIN `country_data`.`country_matrix_2018` b						#UPDATE
ON a.country_code = b.country_code
WHERE a.country_code NOT IN (41, 96, 128, 214)
AND a.year >= b.Start_year
AND a.year <= b.End_year \p;


#########################################################################################################
### Copy GAEZ_penult to ult_hk_etc schema, filtering by country_matrix_2018 and only including hk_etc countries
#########################################################################################################

USE `area_20_ids_MS_ult_hketc` \p;

DROP TABLE IF EXISTS `GAEZ` \p;
CREATE TABLE `GAEZ` like `area_20_ids_MS_sql`.`GAEZ_penult` \p;
INSERT INTO `GAEZ`
SELECT a.* FROM `area_20_ids_MS_sql`.`GAEZ_penult` a
LEFT JOIN `country_data`.`country_matrix_2018` b						#UPDATE
ON a.country_code = b.country_code
WHERE a.country_code IN (41, 96, 128, 214)
AND a.year >= b.Start_year
AND a.year <= b.End_year \p;


###---------------------------------------------------------------###
###---------------------------------------------------------------###
### STEP 5: GLC --------------------------------------------------###
###---------------------------------------------------------------###
###---------------------------------------------------------------###

USE `area_20_ids_MS_raw` \p;

#Populate variables for later use
SET @msg = 'ERROR: UNMAPPED COUNTRY' \p;

#########################################################################################################
### upload GLC land cover values
#########################################################################################################
DROP TABLE IF EXISTS `GLC_raw`  \p;
CREATE TABLE  `GLC_raw`(

  `country` varchar(41) NOT NULL,
   `GLCCROPAREA` float ,
   `GLCPASTAREA` float ,
   `GLCBUILTAREA` float ,
   `GLCFORAREA` float ,
   `GLCBAREAREA` float ,

PRIMARY KEY  (`country`)

) ENGINE=MyISAM DEFAULT CHARSET=latin1  \p;

LOAD DATA LOCAL INFILE "C:\\Users\\Swiader\\Documents\\IDS NFA\\NFA 2020\\2. datasets\\land\\Upload Data\\GLC_raw.csv" 			#UPDATE
INTO TABLE `GLC_raw` CHARACTER SET  latin1
FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY "\r\n" IGNORE 2 LINES
(country, @GLCCROPAREA,@GLCPASTAREA,  @GLCBUILTAREA, @GLCFORAREA, @GLCBAREAREA)

SET
`GLCCROPAREA` =nullif(@GLCCROPAREA,' '),
`GLCPASTAREA` =nullif(@GLCPASTAREA,' '),
`GLCBUILTAREA` =nullif(@GLCBUILTAREA,' '),
`GLCFORAREA` =nullif(@GLCFORAREA,' '),
`GLCBAREAREA` =nullif(@GLCBAREAREA,' ')  \p;

USE `area_20_ids_MS_sql` \p;

DROP TABLE IF EXISTS`GLC`  \p;
CREATE TABLE  `GLC`(
country varchar(40),
item varchar (40),
FAO_item_code int(10),
element varchar (40),
year int(5),
Value FLOAT ,
PRIMARY KEY  (`country`,`year`)
)   \p;
insert into `GLC`
SELECT
country,
'Infrastructure aggregate' as item,
'6661' as FAO_item_code,
'Area (1000 Ha)' as element,
'2000' as year,
`GLCBUILTAREA`/10 as value
from
`area_20_ids_MS_raw`.`GLC_raw`  \p;

#########################################################################################################
###Add country code to GLC data
#########################################################################################################

#2. Add country_code column
ALTER TABLE `GLC` ADD COLUMN `country_code` INTEGER AFTER `country`  \p;

#3A. Map from country_data.country_names_2018 table							#UPDATE
UPDATE `GLC` g
LEFT JOIN country_data.country_names_2018 cn ON cn.name = g.country			#UPDATE
SET g.country_code =  if(cn.code is null,0,cn.code)  \p;

#3B. Delete intentionally dropped countries
DELETE FROM `GLC` WHERE country_code = -1 \p;

#3C. Check to ensure that countries are either correctly mapped or intentionally dropped
SELECT IF ((SELECT count(country_code) FROM `GLC` WHERE country_code = 0) > 0,@msg,'COUNTRY MAPPING OK')  \p;
SELECT DISTINCT country FROM `GLC` WHERE country_code = 0 \p;

#########################################################################################################
###Group and aggregate countries with matching codes
#########################################################################################################

#4A. Create Penult Table


DROP TABLE IF EXISTS `GLC_penult`  \p;
CREATE TABLE  `GLC_penult`(
country varchar(40),
country_code int(5),
item varchar (40),
FAO_item_code int(10),
element varchar (40),
year int(5),
Value FLOAT ,
PRIMARY KEY  (`country`,`year`)
)  \p;

#4B. Fill Ult table with data, aggregate by country code

INSERT INTO `GLC_penult`
SELECT country, country_code, item, FAO_item_code, element, year, SUM(value) from `area_20_ids_MS_sql`.`GLC`
Group by country_code, FAO_item_code, year \p;

#4C. Replace country name strings with primary names

UPDATE `GLC_penult` g
LEFT JOIN country_data.country_names_2018 cn ON cn.code = g.country_code and cn.primary_name = 1		#UPDATE
SET g.country =  cn.name \p;


#########################################################################################################
### calculate for the value between years 1961 and 2000 for artificial surfaces
#########################################################################################################
insert into `GLC_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	d.year,
    a.value * b.value/c.value
FROM
    `GLC_penult` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
	`faostat_18_v1_ult`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE

    a.country_code = b.country_code
		and  a.country_code = c.country_code
        AND a.year = 2000
		and c.year = 2000
        AND a.FAO_item_code ='6661'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>1960
		and d.year<2000
		  \p;

#########################################################################################################
### calculate for the value between years 2001 and 2013 for artificial surfaces
#########################################################################################################
insert into `GLC_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	d.year,
    a.value * b.value/c.value
FROM
    `GLC_penult` a,
    `faostat_18_v1_ult`.`popstat_ult` b,
	`faostat_18_v1_ult`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE
	a.country_code = b.country_code
		and  a.country_code = c.country_code
        AND a.year = 2000
		and c.year = 2000
        AND a.FAO_item_code ='6661'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>2000
		and d.year<=@default_end_year
		  \p;

#########################################################################################################
### calculate for the value between years 1961 and 2000 for artificial surfaces - repeat for HK_etc countries
#########################################################################################################
insert into `GLC_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	d.year,
    a.value * b.value/c.value
FROM
    `GLC_penult` a,
    `faostat_18_v1_ult_hketc`.`popstat_ult` b,
	`faostat_18_v1_ult_hketc`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE

    a.country_code = b.country_code
		and  a.country_code = c.country_code
        AND a.year = 2000
		and c.year = 2000
        AND a.FAO_item_code ='6661'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>1960
		and d.year<2000
		 \p;

#########################################################################################################
### calculate for the value between years 2001 and 2011 for artificial surfaces - repeat for HK_etc countries
#########################################################################################################

insert into `GLC_penult`
SELECT
    a.country,
    a.country_code,
    a.item,
    a.FAO_item_code,
	a.element,
	d.year,
    a.value * b.value/c.value
FROM
    `GLC_penult` a,
    `faostat_18_v1_ult_hketc`.`popstat_ult` b,
	`faostat_18_v1_ult_hketc`.`popstat_ult` c,
	`area_20_ids_MS_sql`.`yearval` d
WHERE
	a.country_code = b.country_code
		and  a.country_code = c.country_code
        AND a.year = 2000
		and c.year = 2000
        AND a.FAO_item_code ='6661'
		and b.subject = 'Total Population - Both sexes'
		and c.subject = 'Total Population - Both sexes'
		and b.year = d.year
		and d.year>2000
		and d.year<=@default_end_year
		 \p;

#########################################################################################################
### Move all countries (except hk_etc) to ult schema
#########################################################################################################

USE `area_20_ids_MS_ult` \p;

DROP TABLE IF EXISTS `GLC` \p;
CREATE TABLE `GLC` like `area_20_ids_MS_sql`.`GLC_penult` \p;
INSERT INTO `GLC`
SELECT a.* FROM `area_20_ids_MS_sql`.`GLC_penult` a
LEFT JOIN `country_data`.`country_matrix_2018` b						#UPDATE
ON a.country_code = b.country_code
WHERE a.country_code NOT IN (41, 96, 128, 214)
AND a.year >= b.Start_year
AND a.year <= b.End_year \p;


#########################################################################################################
### Move China hk_etc countries to hk_etc schema
#########################################################################################################

USE `area_20_ids_MS_ult_hketc` \p;

DROP TABLE IF EXISTS `GLC` \p;
CREATE TABLE `GLC` like `area_20_ids_MS_sql`.`GLC_penult` \p;
INSERT INTO `GLC`
SELECT a.* FROM `area_20_ids_MS_sql`.`GLC_penult` a
LEFT JOIN `country_data`.`country_matrix_2018` b						#UPDATE
ON a.country_code = b.country_code
WHERE a.country_code IN (41, 96, 128, 214)
AND a.year >= b.Start_year
AND a.year <= b.End_year \p;


NOTEE
